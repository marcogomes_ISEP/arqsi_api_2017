﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Medicamentos.Models;
using Microsoft.AspNetCore.Authorization;
using API_Medicamentos.ModelsDTO;

namespace API_Medicamentos.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacoes")]
    public class ApresentacoesController : Controller
    {
        private readonly API_MedicamentosContext _context;

        public ApresentacoesController(API_MedicamentosContext context)
        {
            _context = context;
        }

        // GET: api/Apresentacoes
        [HttpGet("all")]
        public IEnumerable<Apresentacao> GetApresentacoes()
        {
            return _context.Apresentacoes;
        }

        // GET: api/Apresentacoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var apresentacao = await _context.Apresentacoes.Select
            (a => new ApresentacaoDTO()
            {
                id = a.ApresentacaoId,
                forma = a.ApresentacaoForma,
                concentracao = a.ApresentacaoConcentracao,
                qtd = a.ApresentacaoQtd,
                farmacoId = a.FarmacoId.ToString(),
                medicamentoId = a.MedicamentoId.ToString(),
                posologias = a.Posologias.Select(p => new PosologiaDTO() {
                    apresentacaoId = p.ApresentacaoId,
                    posologiaId = p.PosologiaId,
                    duracao = p.duracao,
                    frequencia = p.frequencia,
                    quantidade = p.quantidade,
                    via = p.via
                }),
                comentarios = a.Comentarios.Select(c => new ComentarioDTO() {
                    id = c.ComentarioId,
                    comentario = c.comentario,
                    data = c.data,
                    medicoId = c.medicoId
                })
            })
            .SingleOrDefaultAsync(apresenta => apresenta.id == id);



            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.ApresentacaoId)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacoes.Add(apresentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApresentacao", new { id = apresentacao.ApresentacaoId }, apresentacao);
        }

        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacoes.SingleOrDefaultAsync(m => m.ApresentacaoId == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacoes.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(apresentacao);
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacoes.Any(e => e.ApresentacaoId == id);
        }
    }
}