﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Medicamentos.Models;
using API_Medicamentos.ModelsDTO;
using Microsoft.AspNetCore.Authorization;

namespace API_Medicamentos.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly API_MedicamentosContext _context;

        public FarmacosController(API_MedicamentosContext context)
        {
            _context = context;
        }

        // GET: api/Farmacos/{id}/apresentacoes
        [HttpGet("{id}/apresentacoes")]
        public IActionResult GetApresentacoesByFarmaco([FromRoute] int id)
        {
            var apresentacoes = _context.Apresentacoes.Where(f => f.FarmacoId == id)
                .Select(a => new ApresentacaoDTO()
            {
                id = a.ApresentacaoId,
                medicamentoId = a.MedicamentoId.ToString(),
                farmacoId = a.FarmacoId.ToString(),
                concentracao = a.ApresentacaoConcentracao,
                forma = a.ApresentacaoForma,
                qtd = a.ApresentacaoQtd,
                posologias = a.Posologias.Select(p => new PosologiaDTO()
                    {
                        apresentacaoId = p.ApresentacaoId,
                        posologiaId = p.PosologiaId,
                        duracao = p.duracao,
                        frequencia = p.frequencia,
                        quantidade = p.quantidade,
                        via = p.via
                    }),
                comentarios = a.Comentarios.Select(c => new ComentarioDTO()
                    {
                        id = c.ComentarioId,
                        comentario = c.comentario,
                        data = c.data,
                        medicoId = c.medicoId
                    })
                });

            return Ok(apresentacoes);
        }

        // GET: api/Farmacos/5/Medicamentos
        [HttpGet("{id}/Medicamentos")]
        public IActionResult GetMedicamentos([FromRoute] int id)
        {
            var apresentacoes = _context.Apresentacoes.Where(f => f.FarmacoId == id);


            var medicamentos = from m in apresentacoes.Include(m => m.Medicamento)
                               select new MedicamentoDTO()
                               {
                                   id = m.MedicamentoId,
                                   nome = m.Medicamento.MedicamentoNome
                               };

            return Ok(medicamentos);
        }



        // GET: api/Farmacos/?nome={nome}
        [HttpGet()]
        public async Task<IActionResult> GetFarmaco([FromQuery] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.FarmacoNome.Equals(nome, StringComparison.OrdinalIgnoreCase));


            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.FarmacoId == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.FarmacoId)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.FarmacoId }, farmaco);
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.FarmacoId == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return Ok(farmaco);
        }

        // GET: api/Farmaco/{id}/Posologias
        [HttpGet("{id}/Posologias")]
        public IActionResult GetPosologiasByFarmaco([FromRoute] int id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologias = (from p in _context.Posologias
                              join a in _context.Apresentacoes on p.ApresentacaoId equals a.ApresentacaoId
                              join f in _context.Farmacos on a.FarmacoId equals f.FarmacoId
                              where f.FarmacoId == id
                              select new PosologiaDTO
                              {
                                  posologiaId = p.PosologiaId,
                                  apresentacaoId = p.ApresentacaoId,
                                  duracao = p.duracao,
                                  frequencia = p.frequencia,
                                  quantidade = p.quantidade,
                                  via = p.via
                              });

            return Ok(posologias);

        }


        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.FarmacoId == id);
        }
    }
}