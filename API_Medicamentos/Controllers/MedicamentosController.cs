﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Medicamentos.Models;
using API_Medicamentos.ModelsDTO;
using Microsoft.AspNetCore.Authorization;

namespace API_Medicamentos.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly API_MedicamentosContext _context;

        public MedicamentosController(API_MedicamentosContext context)
        {
            _context = context;
        }


        // GET: api/Medicamentos/{id}/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public IActionResult GetApresentacoesByMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacoes = _context.Apresentacoes.Where    (a => a.MedicamentoId==id)
                .Select(a => new ApresentacaoDTO() {
                    id = a.ApresentacaoId,
                    medicamentoId = a.MedicamentoId.ToString(),
                    farmacoId = a.FarmacoId.ToString(),
                    concentracao = a.ApresentacaoConcentracao,
                    forma = a.ApresentacaoForma,
                    qtd = a.ApresentacaoQtd,
                    posologias = a.Posologias.Select(p => new PosologiaDTO()
                    {
                        apresentacaoId = p.ApresentacaoId,
                        posologiaId = p.PosologiaId,
                        duracao = p.duracao,
                        frequencia = p.frequencia,
                        quantidade = p.quantidade,
                        via = p.via
                    }),
                    comentarios = a.Comentarios.Select(c => new ComentarioDTO()
                    {
                        id = c.ComentarioId,
                        comentario = c.comentario,
                        data = c.data,
                        medicoId = c.medicoId
                    })
                });

            if (apresentacoes == null)
            {
                return NotFound();
            }

            return Ok(apresentacoes);
        }

        // GET: api/Medicamentos/{id}/Posologias
        [HttpGet("{id}/Posologias")]
        public IActionResult GetPosologiasByMedicamento([FromRoute] int id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = _context.Medicamentos.SingleOrDefaultAsync(m => m.MedicamentoId == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            var posologias = (from p in _context.Posologias
                              join a in _context.Apresentacoes on p.ApresentacaoId equals a.ApresentacaoId
                              where a.MedicamentoId == id
                              select new PosologiaDTO {
                                   posologiaId = p.PosologiaId,
                                   apresentacaoId = p.ApresentacaoId,
                                   duracao = p.duracao,
                                   frequencia = p.frequencia,
                                   quantidade = p.quantidade,
                                   via = p.via
                              });

            return Ok(posologias);

        }



        // GET: api/Medicamentos/?nome=
        [HttpGet()]
        public IActionResult GetMedicamento([FromQuery] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (nome==null)
            {
                var medicamentos = from b in _context.Medicamentos
                            select new MedicamentoDTO()
                            {
                                id = b.MedicamentoId,
                                nome = b.MedicamentoNome,
                            };

                return Ok(medicamentos);
            }


            //var medicamento = await _context.Medicamentos.SingleOrDefaultAsync(m => m.MedicamentoNome.Equals(nome));

            var medicamento = _context.Medicamentos.Include(m => m.Apresentacoes)
                .Where(m => m.MedicamentoNome.Equals(nome, StringComparison.OrdinalIgnoreCase));


            if (medicamento == null)
            {
                return NotFound();
            }
           
            return Ok(medicamento);
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var medicamento = await _context.Medicamentos.SingleOrDefaultAsync(m => m.MedicamentoId == id);

            var medicamentos = await _context.Medicamentos.Select
            (b=> new MedicamentoDTO()
            {
                id = b.MedicamentoId,
                nome = b.MedicamentoNome,
            })
            .SingleOrDefaultAsync(b => b.id == id);

            if (medicamentos == null)
            {
                return NotFound();
            }

            return Ok(medicamentos);
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.MedicamentoId)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicamentos.Add(medicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamento", new { id = medicamento.MedicamentoId }, medicamento);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamentos.SingleOrDefaultAsync(m => m.MedicamentoId == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamentos.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(medicamento);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamentos.Any(e => e.MedicamentoId == id);
        }
    }
}