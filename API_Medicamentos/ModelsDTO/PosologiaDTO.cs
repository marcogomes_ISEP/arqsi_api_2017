﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.ModelsDTO
{
    public class PosologiaDTO
    {

        public int posologiaId { get; set; }
        public string via { get; set; }
        public float quantidade { get; set; }
        public string frequencia { get; set; }
        public string duracao { get; set; }
        public int apresentacaoId { get; set; }

    }
}
