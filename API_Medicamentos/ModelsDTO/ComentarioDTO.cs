﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.ModelsDTO
{
    public class ComentarioDTO
    {
        public int id { get; set; }
        public string comentario { get; set; }
        public string medicoId { get; set; }
        public DateTime data { get; set; }
    }
}
