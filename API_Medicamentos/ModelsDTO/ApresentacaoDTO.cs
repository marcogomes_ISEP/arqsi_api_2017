﻿using API_Medicamentos.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class ApresentacaoDTO
    {
        public int id { get; set; }
        public string forma { get; set; }
        public String concentracao { get; set; }
        public String qtd { get; set; }
        public String medicamentoId { get; set; }
        public String farmacoId { get; set; }
        public IEnumerable<PosologiaDTO> posologias { get; set; }
        public IEnumerable<ComentarioDTO> comentarios { get; set; }

    }
}
