﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.ModelsDTO
{
    public class FarmacoDTO
    {
        public int id { get; set; }
        public string nome { get; set; }
    }
}
