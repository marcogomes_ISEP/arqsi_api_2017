﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace API_Medicamentos.Migrations
{
    public partial class comentarios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Comentario",
                columns: table => new
                {
                    ComentarioId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApresentacaoId = table.Column<int>(type: "int", nullable: true),
                    comentario = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    data = table.Column<DateTime>(type: "datetime2", nullable: false),
                    medicoId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comentario", x => x.ComentarioId);
                    table.ForeignKey(
                        name: "FK_Comentario_Apresentacoes_ApresentacaoId",
                        column: x => x.ApresentacaoId,
                        principalTable: "Apresentacoes",
                        principalColumn: "ApresentacaoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Apresentacoes_FarmacoId",
                table: "Apresentacoes",
                column: "FarmacoId");

            migrationBuilder.CreateIndex(
                name: "IX_Comentario_ApresentacaoId",
                table: "Comentario",
                column: "ApresentacaoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacoes_Farmaco_FarmacoId",
                table: "Apresentacoes",
                column: "FarmacoId",
                principalTable: "Farmaco",
                principalColumn: "FarmacoId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacoes_Farmaco_FarmacoId",
                table: "Apresentacoes");

            migrationBuilder.DropTable(
                name: "Comentario");

            migrationBuilder.DropIndex(
                name: "IX_Apresentacoes_FarmacoId",
                table: "Apresentacoes");
        }
    }
}
