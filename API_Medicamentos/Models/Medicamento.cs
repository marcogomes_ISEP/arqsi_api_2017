﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class Medicamento
    {
        public int MedicamentoId { get; set; }
        public string MedicamentoNome { get; set; }
        public ICollection<Apresentacao> Apresentacoes { get; set; }
    }
}
