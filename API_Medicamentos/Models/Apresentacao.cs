﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class Apresentacao
    {
        public int ApresentacaoId { get; set; }
        public string ApresentacaoForma { get; set; }

        public string ApresentacaoConcentracao { get; set; }
        public string ApresentacaoQtd { get; set; }

        public int MedicamentoId { get; set; }

        public Medicamento Medicamento { get; set; }
        public ICollection<Posologia> Posologias { get; set; }
        public ICollection<Comentario> Comentarios { get; set; }

        public int FarmacoId { get; set; }
        public Farmaco Farmaco {get; set;}
    }
}
