﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class Comentario
    {
        public int ComentarioId { get; set; }
        public int apresentacaoId { get; set; }
        public Apresentacao apresentacao { get; set; }
        public string comentario { get; set; }
        public string medicoId { get; set; }
        public DateTime data { get; set; } = DateTime.Now;
        
    }
}
