﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class Posologia
    {

        public int PosologiaId { get; set; }
        public string via { get; set; }
        public float quantidade { get; set; }
        public string frequencia { get; set; }
        public string duracao { get; set; }
        public int ApresentacaoId { get; set; }

        public Apresentacao apresentacao { get; set; }
    }
}
