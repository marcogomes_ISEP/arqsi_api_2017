﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Medicamentos.Models
{
    public class Farmaco
    {
        public int FarmacoId { get; set; }
        public string FarmacoNome { get; set; }

    }
}
