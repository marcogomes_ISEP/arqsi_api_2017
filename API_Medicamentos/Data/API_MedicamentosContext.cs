﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API_Medicamentos.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace API_Medicamentos.Models
{
    public class API_MedicamentosContext : IdentityDbContext<UserEntity>
    {
        public API_MedicamentosContext (DbContextOptions<API_MedicamentosContext> options)
            : base(options)
        {
        }


        public DbSet<Medicamento> Medicamentos { get; set; }
        public DbSet<Apresentacao> Apresentacoes{ get; set; }
        public DbSet<Posologia> Posologias { get; set; }

        public DbSet<Farmaco> Farmacos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Medicamento>().HasMany(s => s.Apresentacoes);
            modelBuilder.Entity<Apresentacao>().HasMany(s => s.Posologias);
            modelBuilder.Entity<Apresentacao>().HasMany(a => a.Comentarios);
            modelBuilder.Entity<Apresentacao>().HasOne(s => s.Farmaco);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<API_Medicamentos.Models.Farmaco> Farmaco { get; set; }

        public DbSet<API_Medicamentos.Models.Comentario> Comentario { get; set; }



    }
}
