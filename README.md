### Create an user:

```http
POST /api/Account HTTP/1.1
Content-Type: application/json
```
```json
{
	"email": "1151071@isep.ipp.pt",
	"password": "ASDasd123_"
}
```

### Request token
```http
POST /api/Account/Token HTTP/1.1
Content-Type: application/json
```
```json
{
	"email": "1151071@isep.ipp.pt",
	"password": "ASDasd123_"
}
```

response:
```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJjYmEwMjYzOC0wN2Q0LTRjMDAtOTBlMi01YWQ1YzQyMmVjOTAiLCJzdWIiOiJydWltc2JhcnJvczA4QGdtYWlsLmNvbSIsImV4cCI6MTUwODAzNzQwOSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo2MTcwNyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NjE3MDcifQ.dUl4dnZpwnHEWK-5-xJuI5tBZsTD-AonJMyhM8cPI3s",
    "expiration": "2017-10-15T03:16:49Z"
}
```

### Request example
```http
GET /api/Medicamentos HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJjYmEwMjYzOC0wN2Q0LTRjMDAtOTBlMi01YWQ1YzQyMmVjOTAiLCJzdWIiOiJydWltc2JhcnJvczA4QGdtYWlsLmNvbSIsImV4cCI6MTUwODAzNzQwOSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo2MTcwNyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NjE3MDcifQ.dUl4dnZpwnHEWK-5-xJuI5tBZsTD-AonJMyhM8cPI3s
```